export class Notification {
    constructor(
        id: number,
        state: string,
        stateNotification: string,
        productId: number,
        orderId: number,
        questionId: number,
        userId: number,
        createdAt: string,
        updatedAt: string,
        serviceId: string,
    ) {}
}
