export class User {
    constructor(
        id: number,
        name: string,
        lastName: string,
        phone: string,
        imageBanner: string,
        imageProfile: string,
        facebook: string,
        twitter: string,
        instragram: string,
        google_: string,
        nameCompany: string,
        businessType: string,
        nif: string,
        businessPhone: string,
        foundationYear: string,
        description: string,
        juridical: boolean,
        status: boolean,
        password: string,
        email: string,
    ) {
    }
}
