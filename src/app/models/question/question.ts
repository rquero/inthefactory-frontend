export class Question {
    constructor(
        id: number,
        description: string,
        state: string,
        canceled: string,
        status: boolean,
        productId: number,
        createdAt: string,
        updatedAt: string,
        userId: number,
        serviceId: number,
    ) {}
}
