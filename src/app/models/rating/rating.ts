export class Rating {
    constructor(
        id: number,
        facility: number,
        human: number,
        service: number,
        description: string,
        userId: number,
        providerId: number,
        orderId: number,
        assessmentProvider: string,
        customerId: number,
    ) {}
}
